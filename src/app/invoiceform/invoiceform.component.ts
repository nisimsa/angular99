import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Invoice} from '../invoice/invoice'

@Component({
  selector: 'app-invoiceform',
  templateUrl: './invoiceform.component.html',
  styleUrls: ['./invoiceform.component.css']
})
export class InvoiceformComponent implements OnInit {

  @Output() invoiceAddedEvent = new EventEmitter<Invoice>();
  invoice:Invoice = {
    name: '',
    amount: ''
  };

  constructor() { }

  onSubmit(form:NgForm){
    console.log(form);
    this.invoiceAddedEvent.emit(this.invoice);
    this.invoice = {
       name: '',
       amount: ''
    }
  }
  ngOnInit() {
  }

}
