import { Component, OnInit,  Output,EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Product} from './product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
   inputs:['product']
})
export class ProductComponent implements OnInit {
product:Product;
tempUser:Product = {pid:null,pname:null,cost:null,categoryid:null};
isEdit : boolean = false;
editButtonText = 'Edit';

@Output() deleteEvent = new EventEmitter<Product>();
@Output() editEvent = new EventEmitter<Product>();

  constructor() { }

   sendDelete(){
    this.deleteEvent.emit(this.product);
  }

    toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
    
     if(this.isEdit){
       this.tempUser.pid = this.product.pid;
       this.tempUser.pname = this.product.pname;
       this.tempUser.cost = this.product.cost;
       this.tempUser.categoryid = this.product.categoryid;
     } else {     
       this.editEvent.emit(this.product);
     }
  }

    cancelEdit(){
    this.isEdit = false;
    this.product.pid = this.tempUser.pid;
    this.product.pname = this.tempUser.pname;
    this.product.cost = this.tempUser.cost;
    this.product.categoryid = this.tempUser.categoryid;
    this.editButtonText = 'Edit'; 
  }

  ngOnInit() {
  }

}
