import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { FormComponent } from './form/form.component';
import { UsersService  } from './users/users.service';
import { AngularFireModule } from 'angularfire2';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InvoiceformComponent } from './invoiceform/invoiceform.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicesService } from './invoices/invoices.service';


const appRoutes: Routes = [
 
  { path: 'invoices', component: InvoicesComponent },
  { path: 'invoicesform', component: InvoiceformComponent },
  { path: '', component: InvoicesComponent },
  { path: '**', component: PageNotFoundComponent }
];

  var config = {
    apiKey: "AIzaSyBHLDVk89q4GcLGc5BYfLErsMZhqZRwvpM",
    authDomain: "angular-test-a5574.firebaseapp.com",
    databaseURL: "https://angular-test-a5574.firebaseio.com",
    storageBucket: "angular-test-a5574.appspot.com",
    messagingSenderId: "496525339881"
  };

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    FormComponent,
    ProductsComponent,
    ProductComponent,
    PageNotFoundComponent,
    InvoiceformComponent,
    InvoicesComponent,
    InvoiceComponent,
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(config),
    RouterModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [UsersService, ProductsService, InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }