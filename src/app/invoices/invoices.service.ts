import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';

@Injectable()
export class InvoicesService {

  invoicesObservable;
  constructor(private af:AngularFire, private _http:Http) { }
  
    addInvoice(invoice){
    this.invoicesObservable.push(invoice);
  }


  getInvoices(){
    this.invoicesObservable = this.af.database.list('/invoices');
    return this.invoicesObservable;
	}

}
