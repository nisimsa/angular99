import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {

invoices;

  constructor(private _invoicesService: InvoicesService) { }


  addInvoice(invoice){
    this._invoicesService.addInvoice(invoice);
  }


  ngOnInit() {    this._invoicesService.getInvoices()
 			    .subscribe(invoices => {this.invoices = invoices;console.log(invoices)});
  }

}

   

